# tdm-gcc

## 介绍
tdm-gcc安装包

## 使用说明

### tdm-gcc 10.3.0版本

**64位下载地址：**[点击下载tdm64-gcc-10.3.0.exe](https://gitee.com/dot2/tdm-gcc/raw/master/tdm64-gcc-10.3.0.exe)

**32位下载地址：**[点击下载tdm-gcc-10.3.0.exe](https://gitee.com/dot2/tdm-gcc/raw/master/tdm-gcc-10.3.0.exe)

> 更新时间：2021年5月25日